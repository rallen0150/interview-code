from random import choice, randint, shuffle  # https://docs.python.org/3.6/library/random.html
import sqlite3  # https://docs.python.org/3.6/library/sqlite3.html
from string import ascii_lowercase, ascii_uppercase, digits, punctuation  # https://docs.python.org/3.6/library/string.html
import requests

db_path = "./users.db"

def generate_password(length: int, complexity: int) -> str:
    """Generate a random password with given length and complexity

    Complexity levels:
        Complexity == 1: return a password with only lowercase chars
        Complexity ==  2: Previous level plus at least 1 digit
        Complexity ==  3: Previous levels plus at least 1 uppercase char
        Complexity ==  4: Previous levels plus at least 1 punctuation char

    :param length: number of characters
    :param complexity: complexity level
    :returns: generated password
    """

    password = ''
    ### append to this list to make sure there are at least one of the necessary requirements in the password
    password_list = []
    password_list.append(choice(ascii_lowercase))
    if complexity > 1:
        password_list.append(choice(digits))
    if complexity > 2:
        password_list.append(choice(ascii_uppercase))
    if complexity > 3:
        password_list.append(choice(punctuation))

    for x in range(length - complexity):
        password_list.append(choice(ascii_lowercase+digits+ascii_uppercase+punctuation))
    ### mix up the order of the password
    shuffle(password_list)
    ### creates the password after being shuffled
    password = "".join(password_list)
    return password

# password = generate_password(7,4)
# print(password)

def check_password_level(password: str) -> int:
    """Return the password complexity level for a given password

    Complexity levels:
        Return complexity 1: If password has only lowercase chars
        Return complexity 2: Previous level condition and at least 1 digit
        Return complexity 3: Previous levels condition and at least 1 uppercase char
        Return complexity 4: Previous levels condition and at least 1 punctuation

    Complexity level exceptions (override previous results):
        Return complexity 2: password has length >= 8 chars and only lowercase chars
        Return complexity 3: password has length >= 8 chars and only lowercase and digits

    :param password: password
    :returns: complexity level
    """
    chk_lower = any(x.islower() for x in password)
    chk_upper = any(x.isupper() for x in password)
    chk_digit = any(x.isdigit() for x in password)
    chk_spec = password.isalnum()

    if chk_lower and chk_digit and chk_upper and (chk_spec == False):
        level = 4
    elif chk_lower and chk_digit and chk_upper:
        level = 3
    elif chk_lower and chk_digit:
        if len(password) >= 8:
            level = 3
        else:
            level = 2
    elif chk_lower:
        if len(password) >= 8:
            level = 2
        else:
            level = 1

    return level

# print(check_password_level(password))

def create_user(db_path: str) -> None:  # you may want to use: http://docs.python-requests.org/en/master/
    """Retrieve a random user from https://randomuser.me/api/
    and persist the user (full name and email) into the given SQLite db

    :param db_path: path of the SQLite db file (to do: sqlite3.connect(db_path))
    :return: None
    """
    url = "https://randomuser.me/api/"
    rand_user = requests.get(url).json()
    ### get the first random user
    first_name = rand_user['results'][0]['name']['first']
    last_name = rand_user['results'][0]['name']['last']
    email = rand_user['results'][0]['email']

    users_db = sqlite3.connect(db_path)
    # users_db.execute("DROP TABLE IF EXISTS users")
    create_table = """
        CREATE TABLE IF NOT EXISTS users (
            id INTEGER PRIMARY KEY,
            first_name VARCHAR(25),
            last_name VARCHAR(40),
            email VARCHAR(50),
            password VARCHAR (20)
        )
    """
    users_db.execute(create_table)
    users_db.execute("INSERT INTO users (first_name, last_name, email) VALUES (?, ?, ?)", (first_name, last_name, email))
    users_db.commit()
    users_db.close()
    return None

# create_user(db_path)
