# Solution
### For my solution I will now explain the process I took to solve this challenge.

The first step to solving this challenge was to first create the function that autogenerates the password.
**generate_password** function:
1. Depending on what the complexity level is for the password, the function always grabs a random lowercase character first.
  * Then based on the complexity level, the function adds another type of character to the password.
  * Whether it's a number, an uppercase letter or a punctuation (special character)
  * The password is first created as a list.
2. As the complexity level continues to grow, there is a list for all the available characters and the digits, uppercase and special characters get added depending on the complexity level.
3. When all the requirements are met for the minimum amount of types of characters, then depending on the length of the password the function randomly choses the remaining characters.
4. When all the characters are selected (based on the length of the password), I have the function shuffle up the list so they are in a random order.
5. After the list is shuffled, then I go and join the list to create a string that becomes the password.
6. When that password is created, I return the password.

My next step was to work on checking the complexity level of the password.
**check_password_level** function:
1. The first part of the function is to create boolean variables that checked every requirements for the 4 different complexity levels.
  * The variables are just if there are digits, uppercase, lowercase and special characters present in the password.
2. After we get all the variables assigned, we start to figure out which level the password belongs in.
  * We start from the highest level of complexity with if-elif-else statements.
    * If all requirements are present, then the complexity level is 4.
    * Else if only lowercase, digits and uppercase characters exist, the complexity level is 3.
    * Else if only lowercase and digits exist, the complexity level is at least 2.
    * Else the complexity level is at least 1.
  * If the only digits/lowercase or only lowercase characters exist, there is another part of the function.
    * Depending on the length of the password, can there be a different complexity level than stated above.
      * Only the passwords that are 8 or more characters will you add 1 to the complexity level.
3. Finally when the level is found, we return the level.

Finally we want to make a sqlite3 database for the users.
**create_user** function:
1. The first step for this function is to create a db file and link the path to that file.
2. Next we grab a random user from the https://randomuser.me/api/ API and we grab all the information for that user.
3. We plug the first name, last name and the email of the user into their respective variables.
4. If the *users* doesn't exist, then we create the table in the file path.
5. Once the table is created, or has been created, we add 1 user to the *users* table with the following fields:
  * ID
  * First Name
  * Last Name
  * Email Address
6. Once the user have been inserted, the function is done.


### Once those functions were done, the next step was to create testing scripts to make sure the functions worked.

**My first script was to test the password and the complexity level.**
**password_test.py** Script:
1. First to create the testing script was to import the 2 functions that I had compeleted from *password_creator.py*.
  * I wanted to test the functions **50** times each to make sure there were different results.
2. I created variables for the script to randomly choose the length of the password between the *min_length* and  *max_length* as well as the complexity level for the generation of the password.
3. Once we have the length and complexity of the password, we run the **generate_password** password to a variable of *password*.
4. When the *password* variable is initialized, we find if the password strength *passes* or *fails*.
5. In order for the password to *pass*, the complexity level that was randomly generated and the level of the password from the **check_password_level** function have to equal. If they don't, then the password *fails*.

**Finally the last test script was for the users and sqlite3.**
**users_test.py** Script:
1. Again we bring in the functions that we need in order to test this script.
  * **generate_password**
  * **create_user**
2. Next we create 10 new user records and insert them in the *users* database through the **create_user** function.
3. Then we grab the first 10 users in the *users* database without a password affiliated to them through a SQL Select statement.
  * *SELECT * FROM users WHERE password IS NULL LIMIT 10*
4. Once we have those 10 users, we run a loop to go through all 10 users and we autogenerate a password for each user from **generate_password**.
5. When the password is created, we update the *users* database to add the password for the individual user depending on their id, name and email.


### Those were my steps to solving this challenge. My introduction video can be found [here](https://drive.google.com/drive/folders/1nqMuHvMItmPRZ9pmJhFXV2jM9LHVNwjk).
