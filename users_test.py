from password_creator import generate_password, create_user
import sqlite3
from random import randint

"""
This test is designed to add to a sqlite3 database.
    - A password will be created
    - A user will be created (through the API call to the randomuser website)
    - The user and the password will be inserted into the local database
10 users will be pulled from the database and 10 passwords will be created and the passwords will be updated to those users
"""

if __name__ == '__main__':
    db_path = './users.db'
    users_query = 'SELECT * FROM users WHERE password IS NULL LIMIT 10'
    db = sqlite3.connect(db_path)
    c = db.cursor()
    for x in range(10):
        create_user(db_path)
    c.execute(users_query)
    users = c.fetchall()
    for x in range(10):
        min_length = 6
        max_length = 12
        complexity = randint(1, 4)
        length = randint(min_length, max_length)

        ### Create the random passwords for the users with random requirements
        password = generate_password(length, complexity)
        ### Grab the individual user from the query
        ind_user = users[x]
        user_id = ind_user[0]
        user_first_name = ind_user[1]
        user_last_name = ind_user[2]
        user_email = ind_user[3]

        update_query = 'UPDATE users SET password = ? WHERE id=? AND first_name=? AND last_name=? AND email=?'
        c.execute(update_query, (password, user_id, user_first_name, user_last_name, user_email))

    db.commit()
    db.close()
