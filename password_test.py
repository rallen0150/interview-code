from random import randint
from password_creator import generate_password, check_password_level

"""
This is to test if my methods generate and checks the password. If the password works, it returns "Success", else it returns "Fails"
To determine if the password is successful is by checking the complexity level to the level of strength of the password
The return statement includes the password, complexity level, assertion level, message
"""

if __name__ == '__main__':
    # runs the 2 functions 50 times
    for x in range(50):
        min_length = 6
        max_length = 12
        # gets the complexity of the password
        complexity = randint(1, 4)
        # length of the password
        length = randint(min_length, max_length)

        # runs the 2 functions
        password = generate_password(length, complexity)
        level = check_password_level(password)

        if level == complexity:
            print((password, complexity, level, "Success"))
        else:
            print((password, complexity, level, "Fail"))
